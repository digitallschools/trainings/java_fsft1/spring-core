/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training;

import com.digitallschool.training.spiders.spring.Publisher;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
//@Repository
public class PublisherRepository {

    private DataSource ds;

    public PublisherRepository(DataSource ds) {
        this.ds = ds;
    }

    public List<Publisher> allPublishers() {
        List<Publisher> publishers = new ArrayList<>();

        try (Connection con = ds.getConnection();
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM publishers")) {

            while (rs.next()) {
                publishers.add(
                        new Publisher(rs.getInt(1),
                                rs.getString(2),
                                rs.getString(5),
                                rs.getString(4),
                                rs.getString(3))
                );
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return publishers;
    }
}
