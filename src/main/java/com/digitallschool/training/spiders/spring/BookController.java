/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.digitallschool.training.BooksRepository;
/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class BookController {

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-conf_4.xml");

        BooksRepository repo = ctx.getBean("BooksRepo1", BooksRepository.class);
        Book b1 = repo.getBook(2);
        System.out.println(b1);
    }
}
