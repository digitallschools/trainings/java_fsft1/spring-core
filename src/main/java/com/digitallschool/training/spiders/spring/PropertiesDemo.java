/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring;

import java.io.FileReader;
import java.util.Properties;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class PropertiesDemo {
    public static void main(String[] args) throws Exception{
        Properties props = new Properties();
        props.load(PropertiesDemo.class.getClassLoader().getResourceAsStream("dbconfig.properties"));
        
        System.out.println(props);
    }
}






