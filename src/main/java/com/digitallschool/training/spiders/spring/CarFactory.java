/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring;

import java.util.Objects;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class CarFactory {

    private static Car temp;

    /*static {
        temp = new Car();
    }*/
    
    public Car carInstance(){
        return new Car();
    }
    
    public static Car getInstance(){
        return new Car();
    }

    public static Car getSingletonInstance() {
        if (Objects.nonNull(temp)) {
            return temp;
        } else {
            temp = new Car();
            return temp;
        }
    }
}
