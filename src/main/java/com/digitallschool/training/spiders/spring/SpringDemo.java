/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring;

import com.digitallschool.training.BooksRepository;
import javax.annotation.Resource;
import javax.sql.DataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Component
public class SpringDemo {

    @Resource
    private BooksRepository repo;

    public void mainSupport() {
        System.out.println("##############REPOSITORY: " + repo.getDataSource());
    }

    public static void main(String[] args) {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("spring-conf_4.xml");
        SpringDemo demo = ctx.getBean(SpringDemo.class);

        demo.mainSupport();
    }

    public static void main12(String[] args) {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("spring-conf_3.xml");

        Car c1 = ctx.getBean("car2", Car.class);

        System.out.println(c1);
    }

    public static void main11(String[] args) {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("spring-conf_2.xml");

        AppAwareBean aab = ctx.getBean("aAB", AppAwareBean.class);

        aab.printBeanNames();
    }

    public static void main10(String[] args) {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("spring-conf.xml");

        Author a1 = ctx.getBean("author1", Author.class);
        System.out.println(a1);

        ctx.registerShutdownHook();
    }

    public static void main9(String[] args) {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("spring-conf.xml");

        Car c1 = ctx.getBean("car7", Car.class);
        System.out.println(c1);
    }

    public static void main8(String[] args) {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("spring-conf.xml");
        Publisher a1 = ctx.getBean("pub3", Publisher.class);

        System.out.println(a1);
    }

    public static void main7(String[] args) {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("spring-conf.xml");

        CollectionsSource t = ctx.getBean("mCS", CollectionsSource.class);

        System.out.println(t.getMyProps());
        System.out.println(t.getMyList());
        System.out.println(t.getMySet());
        System.out.println(t.getMyMap());
    }

    public static void main6(String[] args) {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("spring-conf.xml");

        //String[] beans = ctx.getBeanDefinitionNames();
        String[] beans = ctx.getBeanNamesForType(Publisher.class);

        for (String t : beans) {
            System.out.println(t);
        }

        ctx.registerShutdownHook();
    }

    public static void main5(String[] args) {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("spring-conf.xml");

        Publisher a1 = ctx.getBean("pub3", Publisher.class);
        Publisher a2 = ctx.getBean("pub4", Publisher.class);

        System.out.println(a1);
        System.out.println(a2);

        ctx.registerShutdownHook();
    }

    public static void main4(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-conf.xml");

        Car a1 = ctx.getBean("car5", Car.class);
        System.out.println(a1);

        a1.setBrand("Honda");

        Car a2 = ctx.getBean("car5", Car.class);
        System.out.println(a2);
    }

    public static void main3(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-conf.xml");

        Publisher a1 = ctx.getBean("pub1", Publisher.class);

        System.out.println(a1.getPublisherId());
        System.out.println(a1.getName());

        a1.setPublisherId(225);
        a1.setName("OReilly");
        System.out.println(a1.getPublisherId());
        System.out.println(a1.getName());

        Publisher a2 = ctx.getBean("pub1", Publisher.class);

        System.out.println(a2.getPublisherId());
        System.out.println(a2.getName());
    }

    public static void main2(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-conf.xml");

        Car a = ctx.getBean("car2a", Car.class);

        System.out.println(a);
    }

    public static void main1(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-conf.xml");

        Publisher a1 = ctx.getBean("pub1", Publisher.class);

        System.out.println(a1.getPublisherId());
        System.out.println(a1.getName());

    }
}
