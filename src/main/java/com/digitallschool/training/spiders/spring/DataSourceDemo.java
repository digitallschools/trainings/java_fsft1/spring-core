/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.sql.DataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class DataSourceDemo {

    public static void main(String[] args) throws Exception {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-conf.xml");

        DataSource ds = ctx.getBean("myDS", DataSource.class);

        try (Connection con = ds.getConnection();
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM publishers")) {
            while(rs.next()){
                System.out.println(rs.getInt(1) + " # " + rs.getString(2));
            }
        }

    }
}
