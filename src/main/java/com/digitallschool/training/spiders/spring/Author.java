/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring;

import org.springframework.beans.factory.InitializingBean;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Author implements InitializingBean{
    private int authorId;
    private String name;

    @Override
    public void afterPropertiesSet() throws Exception {
        //System.out.println("Post Construct: " + this);
    }
        
    public Author(){
        super();
    }
    public Author(int authorId, String name){
        this.authorId = authorId;
        this.name = name;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String toString(){
        return "Author[" + authorId + ", " + name + "]";
    }
}
