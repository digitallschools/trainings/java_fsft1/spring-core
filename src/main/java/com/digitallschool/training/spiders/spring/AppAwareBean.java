/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring;

import java.util.Objects;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.Environment;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class AppAwareBean implements ApplicationContextAware{
    private ApplicationContext container;
    
    @Autowired
    private BeanFactory factory;
    
    @Autowired
    private Environment environment;

    @Override
    public void setApplicationContext(ApplicationContext ac) throws BeansException {
        container = ac;
    }
    
    public void printBeanNames(){
        if(Objects.nonNull(container)){
            String[] names = container.getBeanDefinitionNames();
            for(String name: names){
                System.out.println(name);
            }
        }
    }
    
}










