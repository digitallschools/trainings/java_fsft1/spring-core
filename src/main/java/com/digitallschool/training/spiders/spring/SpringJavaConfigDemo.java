/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring;

import com.digitallschool.training.AuthorRepository;
import com.digitallschool.training.PublisherRepository;
import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class SpringJavaConfigDemo {

    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(LibraryConfig.class);
        PublisherRepository publisherRepo = ctx.getBean(PublisherRepository.class);

        List<Publisher> publishers = publisherRepo.allPublishers();
        System.out.println(publishers);
    }

    public static void main2(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(LibraryConfig.class);
        AuthorRepository authorRepo = ctx.getBean(AuthorRepository.class);

        List<Author> authors = authorRepo.getAuthors();
        System.out.println(authors);
    }

    public static void main1(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(LibraryConfig.class);

        Customer c1 = ctx.getBean(Customer.class);
        System.out.println(c1);
        c1.setCustomerId(123);
        System.out.println(c1);

        Customer c2 = ctx.getBean("customerInstance", Customer.class);
        System.out.println(c2);
        c2.setName("James");

        Customer c3 = ctx.getBean("customerInstance", Customer.class);
        System.out.println(c3);
    }
}
