/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Engine {

    private String brand;
    private double price;

    public Engine() {
        brand = "TATA";
        price = 128000;
    }
    
    @PostConstruct
    public void someInit(){
        System.out.println("@PostConstruct: " + this);
    }
    
    @PreDestroy
    public void someDestroy(){
        System.out.println("@PreDestroy: " + this);
    }
    
    public void init(){
        System.out.println("Post Construct: " + this);
    }
    
    public void destroy(){
        System.out.println("Pre Destroy: " + this);
    }

    public Engine(String brand, double price) {
        this.brand = brand;
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    @Override
    public String toString(){
        return "Engine[" + brand + ", " + price + "]";
    }
}
