/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import javax.annotation.Resource;
import javax.sql.DataSource;
import org.springframework.stereotype.Component;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Component("BooksRepo2")
public class BooksRepository {

    @Resource
    private DataSource ds;

    public Book getBook(int bookId) {

        try (Connection con = ds.getConnection();
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM books WHERE book_id=" + bookId)) {
            if (rs.next()) {
                return new Book(rs.getInt(1), rs.getString(2), rs.getShort(3),
                        rs.getDouble(4), rs.getInt(5), rs.getInt(6));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public List<Book> getBooks() {

        return null;
    }

    public DataSource getDataSource() {
        return ds;
    }
}
