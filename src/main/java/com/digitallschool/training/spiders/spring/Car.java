/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring;

import javax.annotation.Resource;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Car {

    private String brand;
    private String color;
    private double price;

    //@Inject
    //@Autowired
    //@Qualifier("engine1")
    @Resource(name = "engine1")
    private Engine engine;

    public Car() {
        brand = "TATA";
        color = "Black";
        price = 595000;
        //engine = new Engine();
    }

    public Car(Engine engine) {
        this.engine = engine;
    }

    public Car(String brand, String color, double price, Engine engine) {
        this.brand = brand;
        this.color = color;
        this.price = price;
        this.engine = engine;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Engine getEngine() {
        return engine;
    }

    //@Required
    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public String toString() {
        return "Car[Brand: " + brand + ", Price: " + price + ", " + engine + "]";
    }
}
