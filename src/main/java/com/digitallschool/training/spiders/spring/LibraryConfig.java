/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring;

import com.digitallschool.training.PublisherRepository;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Configuration
@ComponentScan(basePackages = "com.digitallschool.training")
@PropertySource("classpath:abcd/dbconfig.properties")
public class LibraryConfig {
    
    @Value("${jdbc_driver_class}")
    public String JDBC_DRIVER_CLASS;
    
    @Value("${jdbc_url}")
    String JDBC_URL;

    @Bean
    public PublisherRepository getPublisherRepository() {
        return new PublisherRepository(libraryDataSource());
    }

    @Bean(destroyMethod = "close", initMethod = "")
    public DataSource libraryDataSource() {
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(JDBC_DRIVER_CLASS);
        ds.setUrl(JDBC_URL);
        ds.setUsername("root");
        ds.setPassword("root");

        return ds;
    }

    @Bean()
    @Scope("prototype")
    public Customer customerInstance() {
        return new Customer();
    }

    @Bean
    @Primary
    public Customer defaultCustomer() {
        return new Customer(10, "John", "john@mail.com");
    }
}
