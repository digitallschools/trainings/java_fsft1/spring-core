/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.spring;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Book {

    private int bookId;
    private String title;
    private short edition;
    private double price;
    private int publisherId;
    private int yearOfPublishing;

    public Book() {
        super();
    }

    public Book(int bookId, String title, short edition, double price,
            int yearOfPublishing, int publisherId) {
        this.bookId = bookId;
        this.title = title;
        this.edition = edition;
        this.price = price;
        this.yearOfPublishing = yearOfPublishing;
        this.publisherId = publisherId;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public short getEdition() {
        return edition;
    }

    public void setEdition(short edition) {
        this.edition = edition;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(int publisherId) {
        this.publisherId = publisherId;
    }

    public int getYearOfPublishing() {
        return yearOfPublishing;
    }

    public void setYearOfPublishing(int yearOfPublishing) {
        this.yearOfPublishing = yearOfPublishing;
    }

    @Override
    public String toString() {
        return "Book[" + bookId + ", " + title + ", " + price + ", " + edition + ", " + yearOfPublishing + "]";
    }
}
