/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training;

import com.digitallschool.training.spiders.spring.Author;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.stereotype.Repository;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Repository
public class AuthorRepository {

    private DataSource ds;

    //@Autowired
    public AuthorRepository(DataSource ds) {
        this.ds = ds;
    }

    public List<Author> getAuthors() {
        List<Author> authors = new ArrayList<>();

        try (Connection con = ds.getConnection();
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM authors")) {

            while (rs.next()) {
                authors.add(new Author(rs.getInt(1), rs.getString(2)));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return authors;
    }
}
